# Google Fonts Checker

Checks given url if using google web fonts.

## Requirements

- nodejs

## Dependencies

- [express](http://expressjs.com/)
- [puppeteer](https://pptr.dev/)

## Example HTML

Google Fonts Usage: [https://developers.google.com/fonts/docs/getting_started](https://developers.google.com/fonts/docs/getting_started)

```html
<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine:bold,bolditalic|Inconsolata:italic|Droid+Sans|Open+Sans">
</head>

<body>
    <h1>Google Fonts Example</h1>
</body>
```
