const express = require('express');
const puppeteer = require('puppeteer');

const app = express();

const html = `
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine:bold,bolditalic|Inconsolata:italic|Droid+Sans|Open+Sans">
    </head>
    
    <body>
        <h1>Google Fonts Example</h1>
    </body>
`;

app.get('/', async function (request, response) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    const resources = [];
    const fonts = [];

    // https://pptr.dev/api/puppeteer.pageeventobject.requestfinished
    page.on('requestfinished', request => {
        const resource = request.url();

        // log all resources
        console.log(resource);

        if (resource.indexOf('//fonts.googleapis.com/css') > -1) {
            // https://nodejs.org/api/url.html#the-whatwg-url-api
            const url = new URL(resource);
            const fontFamilies = url.searchParams.get('family').split('|');
            
            resources.push(resource);
            fontFamilies.forEach(fontFamily => {
                fonts.push(fontFamily.replace('+', ' '));
            });
        } 
    });
    
    // switch between following two lines to test the url from request
    // await page.goto(request.query.url, { waitUntil: 'networkidle0' });
    await page.setContent(html, { waitUntil: 'networkidle0' });    
    
    await browser.close();

    const fontsUnique = fonts.filter((font, index) => {
        return fonts.indexOf(font) === index;
    });

    response.json({
        data: {
            resources,
            fonts: fontsUnique
        }
    });
});

app.listen(3000);
